package biz.blachnio.kbrefinement;

import biz.blachnio.kbrefinement.model.Client;
import biz.blachnio.kbrefinement.model.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@RestController
public class ClientRestController {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientRestController(final ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @GetMapping("/clients")
    public List<Client> getAll() {
        final List<Client> clients = new ArrayList<>();
        clientRepository.findAll().forEach(clients::add);
        return clients;
    }

    @GetMapping("/clients/new/{name}")
    public Client insertNewClient(@PathVariable final String name) {
        Client newClient = new Client(name);
        clientRepository.save(newClient);
        return newClient;
    }
}
