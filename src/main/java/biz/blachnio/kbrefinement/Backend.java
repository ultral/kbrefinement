package biz.blachnio.kbrefinement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class Backend {

    @SuppressWarnings("all")// localhost:8080
    @GetMapping("/")
    Map<String, String> home(final HttpServletRequest request) {
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        final Map<String, String> map = new HashMap<String, String>() {{
            put("It works!", baseUrl + "/");
            put("All clients", baseUrl + "/clients");
            put("Add new client", baseUrl + "/clients/new/client_name");
            put("All data", baseUrl + "/data");
            put("Add new data", baseUrl + "/data/new/client_name/code1/code2/someData");
        }};
        return map;
    }

    public static void main(String[] args) {
        SpringApplication.run(Backend.class, args);
    }
}