package biz.blachnio.kbrefinement.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "data")
public class Data {

    @Column(nullable = false)
    private int id;
    @Column(nullable = false)
    private int client_id;
    @EmbeddedId
    private DataPK dataPK;
    private String some_data;
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date created_at;
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date updated_at;

    public Data() {
    }//No default constructor for entity

    public Data(final int clientId, final DataPK dataPK, final String some_data) {
        if (dataPK == null) {
            throw new IllegalArgumentException("dataPK cannot be null");
        }
        this.client_id = clientId;
        this.dataPK = dataPK;
        this.id = dataPK.hashCode();
        this.some_data = some_data;
        this.created_at = new Date();
        this.updated_at = this.created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        if (this.client_id != client_id) {
            this.client_id = client_id;
            this.updated_at = new Date();
        }
    }

    public DataPK getDataPK() {
        return dataPK;
    }

    public void setDataPK(DataPK dataPK) {
        this.dataPK = dataPK;
    }

    public String getSome_data() {
        return some_data;
    }

    public void setSome_data(String some_data) {
        if (!Objects.equals(this.some_data, some_data)) {
            this.some_data = some_data;
            this.updated_at = new Date();
        }
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client_id, dataPK, some_data, created_at, updated_at);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(id, data.id) &&
                Objects.equals(client_id, data.client_id) &&
                Objects.equals(dataPK, data.dataPK) &&
                Objects.equals(some_data, data.some_data) &&
                Objects.equals(created_at, data.created_at) &&
                Objects.equals(updated_at, data.updated_at);
    }


}

