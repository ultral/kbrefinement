package biz.blachnio.kbrefinement.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DataPK implements Serializable {
    private String code1;
    private String code2;

    public DataPK() {}

    public DataPK(String code1, String code2) {
        if (code1 == null) {
            throw new IllegalArgumentException("code1 cannot be null");
        }
        this.code1 = code1;
        if (code2 == null) {
            throw new IllegalArgumentException("code2 cannot be null");
        }
        this.code2 = code2;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code1, code2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataPK dataPK = (DataPK) o;
        return Objects.equals(code1, dataPK.code1) && Objects.equals(code2, dataPK.code2);
    }
}