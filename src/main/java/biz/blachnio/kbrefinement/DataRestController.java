package biz.blachnio.kbrefinement;

import biz.blachnio.kbrefinement.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@RestController
public class DataRestController {

    private final DataRepository dataRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public DataRestController(final DataRepository dataRepository, final ClientRepository clientRepository) {
        this.dataRepository = dataRepository;
        this.clientRepository = clientRepository;
    }

    @GetMapping("/data")
    public List<Data> getAll() {
        final List<Data> data = new ArrayList<>();
        dataRepository.findAll().forEach(data::add);
        return data;
    }

    private Client getClient(final String name) {
        final Client client = clientRepository.findById(name).orElse(new Client(name));
        clientRepository.save(client);
        return client;
    }



    @GetMapping("/data/new/{clientName}/{code1}/{code2}/{someData}")
    public Data insertNewClient(@PathVariable final String clientName, @PathVariable final String code1,
                                  @PathVariable final String code2, @PathVariable final String someData) {
        final Client client = getClient(clientName);
        final DataPK dataPK = new DataPK(code1, code2);
        final Data data = dataRepository.findById(dataPK).orElse(new Data(client.getId(), dataPK, someData));
        data.setClient_id(client.getId());
        data.setSome_data(someData);
        dataRepository.save(data);
        return data;
    }
}
